const SixFlagsSocket = require('./SixFlagsSocket.js');

// Program options
const MIN_CAPACITY = 1; // # of people
const UPDATE_INTERVAL = 30000; // Interval in ms
const TARGET_YEAR = 2021; // Year
const TARGET_MONTH = 6; // Target month as a #
const TARGET_DAY = 9; // Target day as a #
// const LOW_TIME = '12:45 PM';

// Calculated
const TARGET_DATE = new Date(TARGET_YEAR, TARGET_MONTH - 1, TARGET_DAY); // Month 0-indexed

// 12:45 PM
function parseTime(time) {
    let timeArr = time.split(' ');

    let hours = Number(timeArr[0].split(':')[0]);
    let minutes = Number(timeArr[0].split(':')[1]);

    if (timeArr[1] == 'PM' && hours != 12) {
        hours += 12;
    }

    return {
        hours,
        minutes,
    };
}

function timeToMinutes(time) {
    return time.hours + (time.minutes * 60);
}

// Print initial data
console.log('Locating registration for ' + TARGET_DATE.toString());

const socket = new SixFlagsSocket();
socket.onopen = function () {
    console.log('OPEN');
};
socket.oninitializesocket = function (msg) {
    socket.seasonPassActive();
};
socket.ongetcartsummary = function (msg) {
    socket.getMerchantPackageEventDates({
        min_capacity: MIN_CAPACITY,
        month: TARGET_MONTH,
    });
};
socket.onseasonpassactive = function (msg) {};
socket.ongetmerchantpackageeventdates = function (msg) {
    let targetDayDateString = SixFlagsSocket.formatDate(TARGET_DATE);
    let targetDay = msg.D.find((day) => day.date == targetDayDateString);

    if (!targetDay) {
        console.error(`Failed to locate target date ${targetDayDateString}`);
        return;
    }

    let targetTime = targetDay.T.find((time) => time.available >= MIN_CAPACITY);

    if(targetTime) {
        const date = new Date();
        console.log();
        console.log(date.toString());
        console.log(targetTime);
        console.log();
    }

    setTimeout(function () {
        socket.getMerchantPackageEventDates({
            min_capacity: MIN_CAPACITY,
            month: TARGET_MONTH,
        });
    }, UPDATE_INTERVAL);
}
socket.onclose = function (msg) {
    console.log('CLOSE');
}
