const SockJS = require('sockjs-client');

const VERSION = "5.109.0-e";
const APPLICATION_ID = "1500";

class SixFlagsSocket {
    constructor() {
        this.socket = new SockJS('https://sf-nc.secure.sf.accessoticketing.com/api/socket');
        this.socket.onopen = () => {
            if (this.onopen) {
                this.onopen();
            }

            this.initializeSocket();
        };
        let responseHandlerTable = {
            '_initializeSocket': (msg) => {
                if (this.oninitializesocket) {
                    this.oninitializesocket(msg);
                }
            },
            'GetCartSummary': (msg) => {
                if (this.ongetcartsummary) {
                    this.ongetcartsummary(msg);
                }
            },
            'seasonPassActive': (msg) => {
                if (this.onseasonpassactive) {
                    this.onseasonpassactive(msg);
                }
            },
            'GetMerchantPackageEventDates': (msg) => {
                if (this.ongetmerchantpackageeventdates) {
                    this.ongetmerchantpackageeventdates(msg);
                }
            },
        };
        this.socket.onmessage = (e) => {
            let response = JSON.parse(e.data);
            // console.log('message', response);

            let handler = responseHandlerTable[response.request_type];
            if (handler) {
                handler(response);
            } else {
                console.error("Failed to process message type " + response.request_type);
            }
        };
        this.socket.onclose = () => {
            if (this.onclose) {
                this.onclose();
            }
            this.socket = null;
        };

        this.onopen = null;
        this.oninitializesocket = null;
        this.ongetcartsummary = null;
        this.onseasonpassactive = null;
        this.ongetmerchantpackageeventdates = null;
        this.onclose = null;
    }

    initializeSocket() {
        let req = {
            merchant: "SF-NC",
            language: "en",
            device: "desktop",
            hostname: "sf-nc.store.sixflags.com",
            global_api_req_props: [{
                    prop_name: "device",
                    prop_value: "desktop"
                }, {
                    prop_name: "language",
                    prop_value: "en"
                }
            ],
            cart_key: "1180695866",
            cart_id: "207327212",
            session_id: "itmo.234558.930",
            paypal_cancel_url: "https://sf-nc.store.sixflags.com:443/paypalCancelled",
            paypal_return_url: "https://sf-nc.store.sixflags.com:443/paypalComplete",
            skip_requests: "",
            _ua: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36",
            _os: "Windows - 10",
            _device: "desktop - undefined - undefined",
            _browser: "Chrome - 91.0.4472.77",
            _referrer: "https://www.sixflags.com/",
            _location: "https://sf-nc.store.sixflags.com/savingsCalendar/Calendar?keyword=reservations",
            useSimpleFlow: true,
            knowledgebase_id: "3",
            knowledgebase_gateway: "https://knowledgebase.shared.accessoticketing.com/",
            knowledgebase_island: "https://knowledgebase.shared.accessoticketing.com/",
            agent_id: "5",
            user_id: "5",
            machine_id: "500",
            paypal_settings: "EC,PPA",
            island: "SixFlagsV3",
            merchant_id: "30",
            keywords: "Daily Tickets, Season Passes, Memberships, Meal Vouchers, Group Tickets, Flash Passes, Parking, VIP Tour, Animal Encounters, Safari, Swim Observer, Junior Quicksell, Upsell Daily Tickets, Sharks, Parking Quick Sell, Peak, USB, QuicksellSPParking, Calendar, Fright Fest Quicksell, Holiday Dinner Cross Sell, COMP, HIP Tickets, m1, Promo, Dining Pass, Dolphin Discovery, bM4, GoldMembershipAddons, Holiday Dinner, Dining Quicksell, goldsp1, Dinner, Checkout Parking, sp1, SP Upgrade, Holiday, FrightFest, Upsell Gold, Off, goldm1, One Day Parking, Upsell Dolphin Tickets, Donation, AnyDay, Bm1, Career, Digital Photo Upload, Dining Pass Cross Sell, Health, ccff, Ocean, Safari Disc, Teen Truth, DolphinWine, Math, bsp4, Subway, m4, Family Meal, Trainer, Single Meal, Sport Bottles, Pink, SP Discounts, Dining Passreg, Dining PassFF, AnimalGift, Dining PassHIP, hhpass, CalendarQuick, Education, Advance Purchase, Gift, Upsell Indiv Membership, Child Admission, ffhhcalendar, Dine, Deferred, AnimalGiftBuy, cokeff, sp4, Adult Admission, Bgoldm1, Bgoldsp1, gamefly, Bsp1, FFMaze, ScholarShare, SDP1all, Season Pass, Mem Dining Quicksell, SDP4all, Membership Dining, MDP1, bgoldM4, bgoldsp4, Discover Food, MemParkingQuicksell, Coke, SPParkingQuicksell, Regal, Fright Fest Meal, GoldPass4Addons, Fright Fest Meal Quicksell, GoldPassAddons, JF QuickSell, JoshuaFest, md1, MGold Pass, MGold Pass4, MPass, MPass4, MembershipAddons, app-home, Quicksell Memb, sdpall1, Thrill Pass, Sears Promo, SeasonPass4Addons, SeasonPassAddons, Interstitial Season Dining, Interstitial Membership Dining, , Interstitial 4Season Dining, burgerking, cokeffhhcalendar, Daily Ticket, Deposit, DiscoverSP, DoubleFun, goldm1b, goldm1c, goldm1d, goldm1e, goldm4, goldm4b, goldm4c, goldm4d, goldm4e, goldoption, goldsp4, Lapsed, m1b, m1c, m1d, m1e, m4b, m4c, m4d, m4e, mdp1bundle, mdp4bundle, Membership Renewals, mikeandike, MU171CDIV, MU172PQEO, MU173GJNH, MU174LHHA, MU175HOYO, MU176GGHS, MU177SVUY, nmd1, nsdp1, nsdp4, nsdpall, nsdpall4, nsdpff, nsdpff4, Public Safety, Retention, sdp1, sdp4, sdpall4, sdpff4, SP Insider, Trial, InParkPurchase, AnyDayCoke, ThrillPassAddons, AnyDayffhhcalendar, hhattractions, ConfigAddons, QuicksellParking, kids, MembershipAddonsfs, Regular Sport Bottle, Shopping Cross Sell, Attractions,Fun Money, ConfigDining, gsdp1, SeasonPassAddonsfs, Interstitial Membership Dining FS, gmdp1, Cancer, Premium Sport Bottle, Upsell Daily Tickets B, Skycoaster, ConfigParking, ConfigShopping, ConfigMemGoldMisc, ConfigGoldMisc, ConfigMemDining, ConfigMemParking, Mem Dining QS, sdpall2, Discover, review, DiscoverM, Recollection, yzmupsell, Animal Ecounters, season sport bottles, Specials, ConfigMisc, LSPAddons, ConfigMemMisc, dmdp1, LP16AQ7Y6R, KOITOBECDD, KMQILSYFOU, JGWEYYXNBB, ICBIWOJXSZ, HKOBKZIZFV, GYKVPWXFAS, gsdp4a, GROUPON, Go-Kart, Fun Money, FTPWBSZMLO, ECBJPIGAHP, dtpd1, dsdpa, dsdp4a, Dining, CTRQSH88, LP16E843CV, LP16C4W6B7, LP16BH8P9W, CF16sp4, CF16sp1, PreK, CF16m4, CF16m1, CF16gsp4, CF16gsp1, CF16gm4, btpd1, bsdpa, bsdp4a, bsdp1, BQIZUTHTOR, bmdp1, LP16FPUR52, RF164G780P, BAF20169, BAF20168, BAF20167, BAF20166, BAF20165, BAF20164, BAF20163, BAF20162, BAF201611, BAF201610, BAF20161, BAF2016, Attractions, ASFP, All Season Flash Pass, UG20162IOJD, VBMRTY34, WBOYQRLJBT, Wyndham, XPRWTS95, RF161G7Y98, DCZNBZJQMX, PVJLCCSHWK, JZQRBNZZBP, ICEVECMGBL, TJYYDJQVBK, LPNZOYAAIO, EQGLVYJLLY, QuicksellSport, WWADDB, Wyndhamfree, WWUPGRADE, Portuguese Quick Sell, ASPP SP Offer, BAF2017, BAF41461, BAF41463, Birthday, Birthday Quick Sell, DailyBottle, comiccon, DailyFlash, DailyPhoto, Thrill, DTupgrade, BAF41462, WWUPGRADER, WWADDB4, WWADDS, WWADDS4, WWUpgradeB, WWUpgradeS, Mountainmikes, wyndham35, LP16E843CVM, LP16C4W6B7M, LP16FPUR52M, summersale, BAF43630, hhpassflash, mountainmikes1, Promo1, ODDD, ConfigMisc18, ConfigDining18, ConfigMemDining18, hhsppass, ffcalendar, AnyDayffcalendar, maze cross sells, Digital Photo Upload,hhpass, Flash Passes,Digital Photo Upload, HIPSale, memgoldplus1, memgoldplus4, ConfigGoldMemDining19, ConfigGoldMemMisc, BAF2018, BAF48818, BAF49601, BAF49062, BAF49063, ConfigTestA, ConfigTestB, ConfigTestC, ConfigTestD, ConfigTestE, memgoldplus1a, memgoldplus4a, memgoldplus1b, memgoldplus4b, memgoldplus1c, memgoldplus4c, memgoldplus1d, memgoldplus4d, memgoldplus1e, memgoldplus4e, memplatinum4, DI18EXPLORE, GCREDEEM1, DE18REDEEM, DE18EXPLORE, springticket, Employee ASFP, GP18EXPLORE, memgoldplus4nf, memplatinum1, ConfigPlatinumMemMisc, ConfigPlatinumMemDining19, memplatinum1nf, PL18EXPLORE, SFREVIEW2018, memplatinum4nf, memdiamond1, memdiamond1nf, ConfigEliteMemMisc, ConfigEliteMemDining19, ConfigElilteMemMisc, ConfigDiamondMemMisc, ConfigDiamondMemDining19, memdiamond4, memdiamond4nf, memelite1, memelite1nf, memelite4, memelite4nf, memgoldplus1nf, VIP2018, VIP48821, VIP51784, VIP51785, VIP51786, WD30, WD40, WD50a, WDCOMP, WD50b, DW30, DW40, DW50, DCOMP, DSS30, DSS40, DSS50, DSB30, DSB40, DSB50, DIW50, DIS50, DISB50, VIP10, VIP15, VIP20, VIP25, DDDBAFFA, DDDBAFFB, DDDBAFFC, thrilloph, combothrilloph, foodfestpackage, foodfestpackagede, foodfestpackaged, foodfestpackagep, foodfestpackagegp, foodfest12, foodfest12de, foodfest12d, foodfest12p, foodfest12gp, foodfest8, foodfest8de, foodfest8d, foodfest8p, foodfest8gp, foodfestbottle, foodfestbottlede, foodfestbottled, foodfestbottlep, foodfestbottlegp, spmemgoldplus1, spmemplatinum1, spmemdiamond1, spmemelite1, ConfigGoldPIFDining, ConfigPlatinumPIFDining, ConfigDiamondPIFDining, ConfigElitePIFDining, memgoldplus13FM, memgoldplus43FM, memplatinum13FM, memplatinum43FM, memdiamond13FM, memdiamond43FM, memelite13FM, memelite43FM, DailyTicketAddons, ConfigAddons19, ConfigDining19, ConfigMisc19, ConfigFlash, ConfigFlash19, ffsale, fright, Upsell Platinum, Upsell Diamond, Upsell Diamond Elite, memcyber13fm, memcyber43fm, memcyber13fmnf, memcyber43fmnf, memgoldplus13FMNF, memgoldplus43FMNF, memplatinum13FMNF, memplatinum43FMNF, memdiamond13FMNF, memdiamond43FMNF, memelite13FMNF, memelite43FMNF, VIP51787, memgoldplus119, memgoldplus419, memgoldplus1nf19, memgoldplus4nf19, memplatinum119, memplatinum419, memplatinum1nf19, memplatinum4nf19, memdiamond119, memdiamond419, memdiamond1nf19, memdiamond4nf19, memelite119, memelite419, memelite1nf19, memelite4nf19, memgoldplus13FM19, memgoldplus43FM19, memgoldplus13FMnf19, memgoldplus43FMnf19, memplatinum13FM19, memplatinum43FM19, memplatinum13FMnf19, memplatinum43FMnf19, memdiamond13FM19, memdiamond43FM19, memdiamond13FMnf19, memdiamond43FMnf19, memelite13FM19, memelite43FM19, memelite13FMnf19, memelite43FMnf19, memcyber119, memcyber419, memcyber1nf19, memcyber4nf19, memcyber13FM19, memcyber43FM19, memcyber13FMnf19, memcyber43FMnf19, gpbmdining, gpdmdining, gppmdining, gpgmasfp, gppmasfp, pbmdining, ppdmdining, pgmasfp, ppmasfp, dbmdining, dpdmdining, dgmasfp, dpmasfp, debmdining, depdmdining, degmasfp, depmasfp, BAF49061, BAF2019, BAF64962, BAF64963, BAF65243, 20aspp, 20asppfs, 20basdpa, 20basdpafx, 20basdpap, 20basdpb, 20basdpbfx, 20basdpbp, 20basdpfs, 20basdpr, 20combogsp1a, 20combogsp1afx, 20combogsp1b, 20combogsp1bfx, 20combogsp1fxnf, 20combogsp1nf, 20combogsp4a, 20combogsp4b, 20combogsp4nf, 20combosp1a, 20combosp1afx, 20combosp1b, 20combosp1bfx, 20combosp1c, 20combosp1cnf, 20combosp1fs, 20combosp1fsnf, 20combosp1nf, 20combosp1ny, 20combosp1nynf, 20combosp1nyr, 20combosp1nyrnf, 20combosp1ow, 20combosp1ownf, 20combosp1ps, 20combosp1psnf, 20combosp1r, 20combosp1rnf, 20combosp4a, 20combosp4b, 20combosp4c, 20combosp4cnf, 20combosp4nf, 20combosp4ny, 20combosp4nynf, 20combosp4nyrnf, 20combosp4ow, 20combosp4ownf, 20combosp4rny, 20combsp1fxnf, 20dasdpa, 20dasdpafx, 20dasdpb, 20dasdpbfx, 20gasfp, 20gasfpfs, 20gasfpfx, 20memcyber1, 20memcyber13FM, 20memcyber13FMnf, 20memcyber1nf, 20memcyber4, 20memcyber43FM, 20memcyber43FMnf, 20memcyber4nf, 20memdiamond1, 20memdiamond13FM, 20memdiamond13FMnf, 20memdiamond1nf, 20memdiamond4, 20memdiamond43FM, 20memdiamond43FMnf, 20memdiamond4nf, 20memelite1, 20memelite13FM, 20memelite13FMnf, 20memelite1nf, 20memelite4, 20memelite43FM, 20memelite43FMnf, 20memelite4nf, 20memgoldplus1, 20memgoldplus13FM, 20memgoldplus13FMnf, 20memgoldplus1nf, 20memgoldplus4, 20memgoldplus43FM, 20memgoldplus43FMnf, 20memgoldplus4nf, 20memplatinum1, 20memplatinum13FM, 20memplatinum13FMnf, 20memplatinum1nf, 20memplatinum4, 20memplatinum43FM, 20memplatinum43FMnf, 20memplatinum4nf, 20pasdpa, 20pasdpafx, 20pasdpap, 20pasdpb, 20pasdpbfx, 20pasdpbp, 20pasdpfs, 20pasdpr, 20pasfp, 20pasfpfs, 20pasfpfx, 20sp1, 20sp1nf, 20thrill, 20thrillnf, 21aspp, 21asppb, 21basdpap, 21basdpas, 21basdpasfx, 21basdpbp, 21basdpbs, 21basdpbsfx, 21combosp1b, 21combosp1bhf, 21combosp1bhfnf, 21combosp1bhfup, 21combosp1bnf, 21combosp1fx, 21combosp2b, 21combosp2bnf, 21gasfp, 21gasfpfx, 21gasfpow, 21gasfps, 21Other, 21pasdpap, 21pasdpas, 21pasdpasfx, 21pasdpbp, 21pasdpbs, 21pasdpbsfx, 21pasfp, 21pasfpfx, 21pasfpow, 21pasfps, 21sp1afx, anydayzipcompetitive, anydayzipfringe, anydayzipouter, ASPP, BAF2020, BAF66600, BAF66603, BAF66605, BAF66613, BAF66896, BAF66897, BAF66898, BAF74318, baf74895, baf74896, baf74897, baf74898, baf74899, baf74933, baf74934, baf74935, baf74936, baf75175, baf75176, baf75177, baf75855, BAF76286, BAF76287, BAF76288, boo2020, Calendar2, calendardrive, calendarhip, calendarhold, child, Coke2, ConfigDiamondMemDining, ConfigDining20, ConfigDining20ren, ConfigDining21, ConfigEliteMemDining, ConfigFlash20, ConfigFlash20x, ConfigFlash21, ConfigGoldMemDining, ConfigMisc20, ConfigPlatinumMemDining, DailyTicketAddons2, dayzipcompetitive, dayzipfringe, dayzipouter, Extend2019A, Extend2019B, Extend2019C, ExtendGold2019A, ExtendGold2019B, ExtendGold2019C, frightticket, hipdayticket, hipdayticketnf, hipsnacks, holiday40, fudge cross sell, funnel cross sell, merch cross sell, snack cross sell, HolidayUpgradeA, HolidayUpgradeB, HolidayUpgradeC, HP2019AX, junior, mask, MembershipsNF, memdinedbasic, memdineddeluxe, memdinedebasic, memdinededeluxe, memdinegpbasic, memdinegpbasicfp, memdinegpdeluxe, memdinegpdeluxefp, memdinegppremium, memdinegppremiumfp, memdinepbasic, memdinepdeluxe, mwexp, mwexpnf, Parking Quicksell, reservations, reservecomp, reservedaily, reservediamond, reserveelite, reserveemp, reservefooddrive, reservehold, reservemem, reservepass, RETA, RETB, RETC, RETD, RTS25, RTS30, RTS40, RTSSP, SameDay, SameDayCoke, SameDayNF, Season Pass2, senior, thrillcokeaddon, UpgradeHold, upgrademem, Upsell Diamond 3fm, Upsell Diamond Elite 3fm, Upsell Gold 3fm, Upsell Gold 3fmnf, Upsell Gold NF, Upsell Gold3fmn, Upsell Platinum 3fm, vircatgr, vircatind, virdolgr, virdolind, virencounters, virsliongr, virslionind, z2016, z2017, z2018, z2019, z2020, 21basdpf, Renaissance, RENday, virswalrusind, PowerHour, reservecph, cphpkgquick, partner, cphips, chpips, SFDKGROUP, cphips1, cphips2, cphmerch, cphmerch1, cphmerch2",
            request_type: "_initializeSocket",
            _version: VERSION,
            application_id: APPLICATION_ID,
            request_token: "09F3A4D69EEC2AF6A51BD1C641E003EA"
        };

        this.socket.send(JSON.stringify(req));
    }

    seasonPassActive() {
        let req = {
            "media_number": "3604010037636109984641",
            "request_type": "seasonPassActive",
            "_version": VERSION,
            "application_id": APPLICATION_ID,
            "merchant_id": "30",
            "machine_id": "500",
            "agent_id": "5",
            "user_id": "5",
            "request_token": "FB5E8C7E0B8AF9723C4A60556CC47066"
        };

        this.socket.send(JSON.stringify(req));
    }

    getMerchantPackageEventDates(opts) {
        opts.year = opts.year || new Date().getFullYear();
        opts.month = opts.month || -1; // -1 is current month
        
        let req = {
            min_capacity: opts.min_capacity || 1,
            customer_type: "1",
            version: "2",
            extra_movie: "date_time",
            month: opts.month,
            start_date: formatDate(new Date(opts.year, opts.month - 1, 1)),
            end_date: formatDate(new Date(opts.year, opts.month - 1 + 1, 0)),
            display_zero_capacity: "1",
            request_type: "GetMerchantPackageEventDates",
            _version: VERSION,
            application_id: APPLICATION_ID,
            merchant_id: "30",
            machine_id: "500",
            agent_id: "5",
            user_id: "5",
        };

        req.event_id = "100745719";
        // req.event_id = "100745718";

        req.package_id = "10075829";
        //req.package_id = "10081222",

        req.request_token = "FB2509587E583CCED5512EE11CBF66C0";
        // req.request_token = "F915B1ABA31540ACC8DC8E0312D7E915";

        this.socket.send(JSON.stringify(req));
    }
}

function formatDate(date) {
    let year = date.getFullYear().toString();
    let month = (date.getMonth() + 1).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
    return `${year}-${month}-${day}`;
}

module.exports = SixFlagsSocket;
module.exports.formatDate = formatDate;
